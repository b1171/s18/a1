
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);

console.log(trainer.name);
console.log(trainer["pokemon"]);
trainer.talk();

function pokemon(name, level, health, attack) {
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;

	this.tackle = function(target) {
		target.health = target.health - this.attack;
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
	
		if(target.health <=0) {
			this.faint(target);
		}
	}

	this.faint = function(target) {
		console.log(`${target.name} has fainted`)
	}
}

let pikachu = new pokemon("Pikachu", 12, 24, 12);
let geodude = new pokemon("Geodude", 8 , 16, 8);
let mewtwo = new pokemon("Meowtwo", 100, 200, 100);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);